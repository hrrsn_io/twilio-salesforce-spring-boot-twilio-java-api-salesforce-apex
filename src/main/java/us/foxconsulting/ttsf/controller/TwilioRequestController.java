package us.foxconsulting.ttsf.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import us.foxconsulting.ttsf.model.Call;
import us.foxconsulting.ttsf.service.SalesforceService;
import us.foxconsulting.ttsf.service.TwilioResponseBuilder;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@RestController
public class TwilioRequestController {

    @Autowired
    SalesforceService sfService;
    @Autowired
    TwilioResponseBuilder responseService;

    private final Logger logger = LogManager.getLogger();

    @RequestMapping(value = "/welcome")
    public void welcome(@RequestParam Map<String, String> paramMap, HttpServletResponse response) {
        try {
            response.setContentType("application/xml");
            response.getWriter().print(responseService.welcome(paramMap).toXML());
        } catch (IOException e) {
            logger.fatal(e.getMessage());
        }
    }

    @RequestMapping(value = "/gatherDigits")
    public void gatherInput(@RequestParam Map<String, String> paramMap, HttpServletResponse response) {
        try {
            response.setContentType("application/xml");
            response.getWriter().print(responseService.gather(paramMap).toXML());
        } catch (IOException e) {
            logger.fatal(e.getMessage());
        }
    }

    @RequestMapping(value = "/verifyDigits")
    public void verifyDigits(Call call, HttpServletResponse response) {
        try {
            response.setContentType("application/xml");
            response.getWriter().print(responseService.verifyDigits(call).toXML());
        } catch (IOException e) {
            logger.fatal(e.getMessage());
        }
    }

    @RequestMapping(value = "/afterDial")
    public void afterDial(Call call, HttpServletResponse response) {
        sfService.afterDial(call);
        try {
            response.setContentType("application/xml");
            response.getWriter().print(responseService.afterDial().toXML());
        } catch (IOException e) {
            logger.fatal(e.getMessage());
        }
    }

    @RequestMapping(value = "/voicemail")
    public void voicemail(Call call) {
        sfService.logNotFound(call, "Voicemail", true);
    }

    @RequestMapping(value = "/noResponseFromCaller")
    public void noResponseFromCaller(HttpServletResponse response) {
        try {
            response.setContentType("application/xml");
            response.getWriter().print(responseService.noResponseAtWelcome().toXML());
        } catch (IOException e) {
            logger.fatal(e.getMessage());
        }
    }

    @RequestMapping(value = "/statusCallback")
    public void statusCallback(Call call) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            logger.fatal(e.getMessage());
        }
        sfService.statusCallback(call, "Caller hung up");
    }


}
