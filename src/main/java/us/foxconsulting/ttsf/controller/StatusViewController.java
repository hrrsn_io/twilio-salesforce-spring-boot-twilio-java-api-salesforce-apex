package us.foxconsulting.ttsf.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import us.foxconsulting.ttsf.service.DataService;

@RestController
public class StatusViewController {

    @Autowired
    DataService dataService;

    private final Logger logger = LogManager.getLogger();

    @RequestMapping("/")
    public String index() {
        return dataService.printQueueStatus();


    }
}
