package us.foxconsulting.ttsf.helper;

import com.sforce.soap.partner.sobject.SObject;
import us.foxconsulting.ttsf.model.CallAttempt;
import us.foxconsulting.ttsf.model.Call;

/**
 * Created by hgm12 on 1/17/2016.
 */
public class SfObjectMapper {

    public SObject mapCallConnection(Call call) {
        SObject sObj = new SObject();
        sObj.setType("SmartAd__c");
        sObj.setField("Recording_URL__c", call.getRecordingUrl());
        sObj.setField("To_State__c", call.getToState());
        sObj.setField("Dial_Call_Status__c", call.getDialCallStatus());
        sObj.setField("Caller_Country__c", call.getCallerCountry());
        sObj.setField("Direction__c", call.getDirection());
        sObj.setField("Caller_State__c", call.getCallerState());
        sObj.setField("To_Zip__c", call.getToZip());
        sObj.setField("Dial_Call_Sid__c", call.getDialCallSid());
        sObj.setField("Call_Sid__c", call.getCallSid());
        sObj.setField("To__c", call.getTo());
        sObj.setField("Caller_Zip__c", call.getCallerZip());
        sObj.setField("To_Country__c", call.getToCountry());
        sObj.setField("Api_Version__c", call.getApiVersion());
        sObj.setField("Called_Zip__c", call.getCalledZip());
        sObj.setField("Called_City__c", call.getCalledCity());
        sObj.setField("Call_Status__c", call.getCallStatus());
        sObj.setField("Recording_Sid__c", call.getRecordingSid());
        sObj.setField("From__c", call.getFrom());
        sObj.setField("Account_Sid__c", call.getAccountSid());
        sObj.setField("Dial_Call_Duration__c", call.getDialCallDuration());
        sObj.setField("Called_Country__c", call.getCalledCountry());
        sObj.setField("Caller_City__c", call.getCallerCity());
        sObj.setField("Caller__c", call.getCaller());
        sObj.setField("Caller_Name__c", call.getCallerName());
        sObj.setField("From_Country__c", call.getFromCountry());
        sObj.setField("To_City__c", call.getToCity());
        sObj.setField("From_City__c", call.getFromCity());
        sObj.setField("Called_State__c", call.getCalledState());
        sObj.setField("From_Zip__c", call.getFromZip());
        sObj.setField("From_State__c", call.getFromState());
        sObj.setField("Recording_Duration__c", call.getRecordingDuration());
        sObj.setField("MLS__c", call.getMls());
        sObj.setField("Reason__c", call.getReason());
        sObj.setField("Successful_Call__c", call.isSuccessfulCall());
        sObj.setField("Engagement__c", call.getEngagement());

        return sObj;
    }

    public SObject mapCallAttempt(CallAttempt call) {
        SObject sObj = new SObject();
        sObj.setType("Call_Attempt__c");

        sObj.setField("Msg__c", call.getMsg());
        sObj.setField("Called__c", call.getCalled());
        sObj.setField("Digits__c", call.getDigits());
        sObj.setField("To_State__c", call.getToState());
        sObj.setField("Caller_Country__c", call.getCallerCountry());
        sObj.setField("Direction__c", call.getDirection());
        sObj.setField("Caller_State__c", call.getCallerState());
        sObj.setField("To_Zip__c", call.getToZip());
        sObj.setField("Call_Sid__c", call.getCallSid());
        sObj.setField("To__c", call.getTo());
        sObj.setField("Caller_Zip__c", call.getCallerZip());
        sObj.setField("To_Country__c", call.getToCountry());
        sObj.setField("Api_Version__c", call.getApiVersion());
        sObj.setField("Called_Zip__c", call.getCalledZip());
        sObj.setField("Called_City__c", call.getCalledCity());
        sObj.setField("Call_Status__c", call.getCallStatus());
        sObj.setField("From__c", call.getFrom());
        sObj.setField("Account_Sid__c", call.getAccountSid());
        sObj.setField("Called_Country__c", call.getCalledCountry());
        sObj.setField("Caller_City__c", call.getCallerCity());
        sObj.setField("Caller__c", call.getCaller());
        sObj.setField("From_Country__c", call.getFromCountry());
        sObj.setField("To_City__c", call.getToCity());
        sObj.setField("From_City__c", call.getFromCity());
        sObj.setField("Called_State__c", call.getCalledState());
        sObj.setField("From_Zip__c", call.getFromZip());
        sObj.setField("From_State__c", call.getFromState());
        sObj.setField("Attempted_Mls__c", call.getAttemptedMls());
        sObj.setField("Reason__c", call.getReason());

        return sObj;
    }


}
