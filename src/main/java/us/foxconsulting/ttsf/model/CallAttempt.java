package us.foxconsulting.ttsf.model;

/**
 * Created by hgm12 on 12/31/2015.
 */
public class CallAttempt {

    // Twilio default callback
    private String msg;
    private String called;
    private String digits;
    private String toState;
    private String callerCountry;
    private String direction;
    private String callerState;
    private String toZip;
    private String callSid;
    private String to;
    private String callerZip;
    private String toCountry;
    private String apiVersion;
    private String calledZip;
    private String calledCity;
    private String callStatus;
    private String from;
    private String accountSid;
    private String calledCountry;
    private String callerCity;
    private String caller;
    private String fromCountry;
    private String toCity;
    private String fromCity;
    private String calledState;
    private String fromZip;
    private String fromState;

    // Salesforce specific
    private String attemptedMls;
    private String reason;

    public String getCalledCity() {
        return calledCity;
    }

    public void setCalledCity(String calledCity) {
        this.calledCity = calledCity;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCalled() {
        return called;
    }

    public void setCalled(String called) {
        this.called = called;
    }

    public String getDigits() {
        return digits;
    }

    public void setDigits(String digits) {
        this.digits = digits;
    }

    public String getToState() {
        return toState;
    }

    public void setToState(String toState) {
        this.toState = toState;
    }

    public String getCallerCountry() {
        return callerCountry;
    }

    public void setCallerCountry(String callerCountry) {
        this.callerCountry = callerCountry;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getCallerState() {
        return callerState;
    }

    public void setCallerState(String callerState) {
        this.callerState = callerState;
    }

    public String getToZip() {
        return toZip;
    }

    public void setToZip(String toZip) {
        this.toZip = toZip;
    }

    public String getCallSid() {
        return callSid;
    }

    public void setCallSid(String callSid) {
        this.callSid = callSid;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getCallerZip() {
        return callerZip;
    }

    public void setCallerZip(String callerZip) {
        this.callerZip = callerZip;
    }

    public String getToCountry() {
        return toCountry;
    }

    public void setToCountry(String toCountry) {
        this.toCountry = toCountry;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getCalledZip() {
        return calledZip;
    }

    public void setCalledZip(String calledZip) {
        this.calledZip = calledZip;
    }

    public String getCallStatus() {
        return callStatus;
    }

    public void setCallStatus(String callStatus) {
        this.callStatus = callStatus;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getAccountSid() {
        return accountSid;
    }

    public void setAccountSid(String accountSid) {
        this.accountSid = accountSid;
    }

    public String getCalledCountry() {
        return calledCountry;
    }

    public void setCalledCountry(String calledCountry) {
        this.calledCountry = calledCountry;
    }

    public String getCallerCity() {
        return callerCity;
    }

    public void setCallerCity(String callerCity) {
        this.callerCity = callerCity;
    }

    public String getCaller() {
        return caller;
    }

    public void setCaller(String caller) {
        this.caller = caller;
    }

    public String getFromCountry() {
        return fromCountry;
    }

    public void setFromCountry(String fromCountry) {
        this.fromCountry = fromCountry;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getCalledState() {
        return calledState;
    }

    public void setCalledState(String calledState) {
        this.calledState = calledState;
    }

    public String getFromZip() {
        return fromZip;
    }

    public void setFromZip(String fromZip) {
        this.fromZip = fromZip;
    }

    public String getFromState() {
        return fromState;
    }

    public void setFromState(String fromState) {
        this.fromState = fromState;
    }

    public String getAttemptedMls() {
        return attemptedMls;
    }

    public void setAttemptedMls(String attemptedMls) {
        this.attemptedMls = attemptedMls;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

}
