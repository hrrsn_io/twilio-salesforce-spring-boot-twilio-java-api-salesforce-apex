package us.foxconsulting.ttsf.model;

/**
 * Created by hgm12 on 1/17/2016.
 */
public class Call {

    private String accountSid;
    private String apiVersion;
    private String callSid;
    private String callStatus;
    private String calledCity;
    private String calledCountry;
    private String calledState;
    private String calledZip;
    private String caller;
    private String callerName;
    private String callerCity;
    private String callerCountry;
    private String callerState;
    private String callerZip;
    private String dialCallDuration;
    private String dialCallSid;
    private String dialCallStatus;
    private String digits;
    private String direction;
    private String engagement;
    private String from;
    private String fromCity;
    private String fromCountry;
    private String fromState;
    private String fromZip;
    private String mls;
    private String reason;
    private String recordingDuration;
    private String recordingSid;
    private String recordingUrl;
    private boolean successfulCall;
    private String to;
    private String toCity;
    private String toCountry;
    private String toState;
    private String toZip;

    public String getEngagement() {
        return engagement;
    }

    public void setEngagement(String engagement) {
        this.engagement = engagement;
    }

    public boolean isSuccessfulCall() {
        return successfulCall;
    }

    public void setSuccessfulCall(boolean successfulCall) {
        this.successfulCall = successfulCall;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDigits() {
        return digits;
    }

    public void setDigits(String digits) {
        this.digits = digits;
    }

    public String getMls() {
        return mls;
    }

    public void setMls(String mls) {
        this.mls = mls;
    }

    public String getCallStatus() {
        return callStatus;
    }

    public void setCallStatus(String callStatus) {
        this.callStatus = callStatus;
    }

    public String getRecordingUrl() {
        return recordingUrl;
    }

    public void setRecordingUrl(String recordingUrl) {
        this.recordingUrl = recordingUrl;
    }

    public String getToState() {
        return toState;
    }

    public void setToState(String toState) {
        this.toState = toState;
    }

    public String getDialCallStatus() {
        return dialCallStatus;
    }

    public void setDialCallStatus(String dialCallStatus) {
        this.dialCallStatus = dialCallStatus;
    }

    public String getCallerCountry() {
        return callerCountry;
    }

    public void setCallerCountry(String callerCountry) {
        this.callerCountry = callerCountry;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getCallerState() {
        return callerState;
    }

    public void setCallerState(String callerState) {
        this.callerState = callerState;
    }

    public String getToZip() {
        return toZip;
    }

    public void setToZip(String toZip) {
        this.toZip = toZip;
    }

    public String getDialCallSid() {
        return dialCallSid;
    }

    public void setDialCallSid(String dialCallSid) {
        this.dialCallSid = dialCallSid;
    }

    public String getCallSid() {
        return callSid;
    }

    public void setCallSid(String callSid) {
        this.callSid = callSid;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getCallerZip() {
        return callerZip;
    }

    public void setCallerZip(String callerZip) {
        this.callerZip = callerZip;
    }

    public String getToCountry() {
        return toCountry;
    }

    public void setToCountry(String toCountry) {
        this.toCountry = toCountry;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getCalledZip() {
        return calledZip;
    }

    public void setCalledZip(String calledZip) {
        this.calledZip = calledZip;
    }

    public String getCalledCity() {
        return calledCity;
    }

    public void setCalledCity(String calledCity) {
        this.calledCity = calledCity;
    }

    public String getRecordingSid() {
        return recordingSid;
    }

    public void setRecordingSid(String recordingSid) {
        this.recordingSid = recordingSid;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getAccountSid() {
        return accountSid;
    }

    public void setAccountSid(String accountSid) {
        this.accountSid = accountSid;
    }

    public String getDialCallDuration() {
        return dialCallDuration;
    }

    public void setDialCallDuration(String dialCallDuration) {
        this.dialCallDuration = dialCallDuration;
    }

    public String getCalledCountry() {
        return calledCountry;
    }

    public void setCalledCountry(String calledCountry) {
        this.calledCountry = calledCountry;
    }

    public String getCallerCity() {
        return callerCity;
    }

    public void setCallerCity(String callerCity) {
        this.callerCity = callerCity;
    }

    public String getCaller() {
        return caller;
    }

    public void setCaller(String caller) {
        this.caller = caller;
    }

    public String getFromCountry() {
        return fromCountry;
    }

    public void setFromCountry(String fromCountry) {
        this.fromCountry = fromCountry;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getCalledState() {
        return calledState;
    }

    public void setCalledState(String calledState) {
        this.calledState = calledState;
    }

    public String getFromZip() {
        return fromZip;
    }

    public void setFromZip(String fromZip) {
        this.fromZip = fromZip;
    }

    public String getFromState() {
        return fromState;
    }

    public void setFromState(String fromState) {
        this.fromState = fromState;
    }

    public String getRecordingDuration() {
        return recordingDuration;
    }

    public void setRecordingDuration(String recordingDuration) {
        this.recordingDuration = recordingDuration;
    }

    public String getCallerName() {
        return callerName;
    }

    public void setCallerName(String callerName) {
        this.callerName = callerName;
    }

}
