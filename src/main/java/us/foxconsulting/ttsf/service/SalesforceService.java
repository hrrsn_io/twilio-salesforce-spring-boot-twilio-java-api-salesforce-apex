package us.foxconsulting.ttsf.service;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.bind.XmlObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import us.foxconsulting.ttsf.dao.SalesforceDao;
import us.foxconsulting.ttsf.helper.SfObjectMapper;
import us.foxconsulting.ttsf.model.Call;

import java.util.ArrayList;

/**
 * Created by hgm12 on 1/13/2016.
 */
@Service
public class SalesforceService {

    @Autowired
    SalesforceDao sfDao;
    @Autowired
    DataService dataService;

    public ArrayList<String> getSellerInfo(String digits) {
        ArrayList<String> resultForBuilder = new ArrayList();
        try {
            QueryResult queryResults = sfDao.selectOnEngagement(digits);
            if (queryResults.getSize() > 0) {
                for (SObject s : queryResults.getRecords()) {
                    XmlObject obj = s.getChild("Smart_Phone_Number__r");
                    resultForBuilder.add(obj.getChild("X10_Digits__c").getValue().toString());
                    resultForBuilder.add(s.getField("Id").toString());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultForBuilder;
    }

    public void afterDial(Call call) {
        String caller = call.getCaller();
        call.setMls(dataService.getFromDigitsQueue(caller));
        call.setEngagement(dataService.getFromEngagementQueue(caller));
        call.setSuccessfulCall(true);
        dataService.removeFromAllQueues(caller);
        SfObjectMapper mapper = new SfObjectMapper();
        SObject[] sObjArray = {mapper.mapCallConnection(call)};
        sfDao.insertSfObject(sObjArray);
    }

    public void logNotFound(Call call, String reason, boolean endSession) {
        if (call.getMls() == null) {
            call.setMls(dataService.getFromDigitsQueue(call.getCaller()));
        }
        if (endSession) dataService.removeFromAllQueues(call.getCaller());
        call.setSuccessfulCall(false);
        call.setReason(reason);
        SfObjectMapper mapper = new SfObjectMapper();
        SObject[] sObjArray = {mapper.mapCallConnection(call)};
        sfDao.insertSfObject(sObjArray);
    }

    public void statusCallback(Call call, String reason) {
        if (dataService.inAnyQueue(call.getCaller())) {
            logNotFound(call, reason, true);
        }
    }

    public ArrayList<String> resultFromProperty(String digits) {
        ArrayList<String> resultForBuilder = new ArrayList<>();
        QueryResult queryResults = sfDao.selectOnProperty(digits);
        if (queryResults.getSize() > 0) {
            for (SObject s : queryResults.getRecords()) {
                resultForBuilder.add(s.getField("Address_1__c").toString());
                resultForBuilder.add(s.getField("Agent_Number__c").toString());
            }
        }
        return resultForBuilder;
    }

    public void resultFromCallLog() {
        QueryResult queryResults = sfDao.selectOnCallLog();
        if (queryResults.getSize() > 0) {
            for (SObject s : queryResults.getRecords()) {
                System.out.println(s.toString());
            }
        }
    }

    public void fileUpload() {
        SObject s = new SObject();

    }
}



















