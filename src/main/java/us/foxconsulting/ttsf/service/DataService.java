package us.foxconsulting.ttsf.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.appender.SyslogAppender;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by hgm12 on 1/12/2016.
 */
@Service
public class DataService {

    private ConcurrentMap<String, String> digitsQueue = new ConcurrentHashMap<>();
    private ConcurrentMap<String, Integer> currentCalls = new ConcurrentHashMap<>();

    private ConcurrentMap<String, String> engagementQueue = new ConcurrentHashMap<>();

    private final Logger logger = LogManager.getLogger();

    public void putToCurrentCalls(String caller) {
        if (currentCalls.containsKey(caller)) {
            currentCalls.put(caller, currentCalls.get(caller) + 1);
        } else currentCalls.put(caller, 1);
    }

    public Integer getCallCount(String caller) {
        int callCount = 0;
        try {
            callCount = currentCalls.get(caller);
        } catch (Exception e) {
            logger.fatal(e.getMessage());
        }
        return callCount;
    }

    public void putToDigitsQueue(String caller, String digits) {
        digitsQueue.put(caller, digits);
    }

    public String getFromDigitsQueue(String caller) {
        String callerFromQueue = null;
        try {
            callerFromQueue = digitsQueue.get(caller);
        } catch (Exception e) {
            logger.fatal(e.getMessage());
        }
        return callerFromQueue;
    }

    public void putToEngagementQueue(String caller, String engagementId) {
        engagementQueue.put(caller, engagementId);
    }

    public String getFromEngagementQueue(String caller) {
        String engagementIdFromQueue = null;
        try {
            engagementIdFromQueue = engagementQueue.get(caller);
        } catch (Exception e) {
            logger.fatal(e.getMessage());
        }
        return engagementIdFromQueue;
    }

    public void removeFromDigitsQueue(String caller) {
        digitsQueue.remove(caller);
    }

    public void removeFromCurrentCalls(String caller) {
        currentCalls.remove(caller);
    }

    public void removeFromEngagementQueue(String caller) {
        engagementQueue.remove(caller);
    }

    public void removeFromAllQueues(String caller) {
        currentCalls.remove(caller);
        digitsQueue.remove(caller);
        engagementQueue.remove(caller);
    }

    public void removeFromCallsAndDigits(String caller) {
        currentCalls.remove(caller);
        digitsQueue.remove(caller);
    }

    public boolean inAnyQueue(String caller){
        return (currentCalls.containsKey(caller) || digitsQueue.containsKey(caller) || engagementQueue.containsKey(caller));
    }

    public String printQueueStatus(){
        return "Digits Queue:" + System.lineSeparator() + digitsQueue + System.lineSeparator() +
                "Current Calls:" + System.lineSeparator() + currentCalls + System.lineSeparator() +
                "Engagement Queue:" + System.lineSeparator() + engagementQueue;
    }

}
