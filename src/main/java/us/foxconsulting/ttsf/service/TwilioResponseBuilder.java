package us.foxconsulting.ttsf.service;

import com.twilio.sdk.verbs.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import us.foxconsulting.ttsf.model.Call;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by hgm12 on 1/12/2016.
 */
@Service
public class TwilioResponseBuilder {

    @Autowired
    DataService dataService;
    @Autowired
    SalesforceService sfService;
    @Value("${baseUrl}")
    private String ROOT_URL;

    private final Logger logger = LogManager.getLogger();

    public TwiMLResponse welcome(Map<String, String> paramMap) {
        String caller = paramMap.get("From");
        dataService.putToCurrentCalls(caller);
        if (dataService.getCallCount(caller) > 3) {
            return sendToVoicemail("You have reached the maximum attempts .");
        }
        TwiMLResponse twiml = new TwiMLResponse();
        Say please = new Say("Please enter the last 6 digits of the Emm Elle Ess number.");
        please.setVoice("woman");
        Gather gather = new Gather();
        Pause pause = new Pause();
        pause.setLength(12);
        gather.setAction(ROOT_URL + "/gatherDigits");
        gather.setNumDigits(6);
        gather.setTimeout(12);
        try {
            gather.append(please);
            gather.append(pause);
            gather.append(please);
            gather.append(pause);
            gather.append(please);
            twiml.append(gather);
            twiml.append(new Redirect(ROOT_URL + "/noResponseFromCaller"));
        } catch (TwiMLException e) {
            logger.fatal(e.getMessage());
        }
        return twiml;
    }

    public TwiMLResponse gather(Map<String, String> paramMap) {
        String digits = paramMap.get("Digits");
        String caller = paramMap.get("Caller");
        dataService.putToDigitsQueue(caller, digits);
        TwiMLResponse twiml = new TwiMLResponse();
        if (digits.length() != 6){
            try {
                Say say = new Say("You did not enter 6 digits . ");
                say.setVoice("woman");
                twiml.append(say);
                twiml.append(new Redirect(ROOT_URL + "/welcome"));
            } catch (TwiMLException e) {
                e.printStackTrace();
            }
            if (dataService.getCallCount(caller) < 3) dataService.removeFromDigitsQueue(caller);
            return twiml;
        }
        Say verifyDigits = new Say("You entered " + readDigits(digits) + ", Press one to confirm, or press two to re enter number.");
        verifyDigits.setVoice("woman");
        Pause pause = new Pause();
        pause.setLength(12);
        Gather gather = new Gather();
        gather.setAction(ROOT_URL + "/verifyDigits");
        gather.setNumDigits(1);
        gather.setTimeout(12);
        try {
            gather.append(verifyDigits);
            gather.append(pause);
            gather.append(verifyDigits);
            gather.append(pause);
            gather.append(verifyDigits);
            twiml.append(gather);
            twiml.append(new Redirect(ROOT_URL + "/noResponseFromCaller"));
        } catch (TwiMLException e) {
            logger.fatal(e.getMessage());
        }
        return twiml;
    }

    public TwiMLResponse verifyDigits(Call call) {
        TwiMLResponse twiml = new TwiMLResponse();
        String caller = call.getCaller();
        call.setMls(dataService.getFromDigitsQueue(caller));

        if (!call.getDigits().contains("1")) {
            try {
                twiml.append(new Redirect(ROOT_URL + "/welcome"));
            } catch (TwiMLException e) {
                logger.fatal(e.getMessage());
            }
            if (dataService.getCallCount(caller) < 3) dataService.removeFromDigitsQueue(caller);
            return twiml;
        }

        ArrayList<String> sfResult = sfService.getSellerInfo(call.getMls());
        try {
            if (sfResult.size() > 0) {
                dataService.putToEngagementQueue(call.getCaller(), sfResult.get(1));
                dataService.removeFromCurrentCalls(call.getCaller());
                Say say = new Say("Please hold while I connect your call" +
                        " . This call may be monitored or recorded for training purposes. ");
                say.setVoice("woman");
                twiml.append(say);
                Dial dial = new Dial(sfResult.get(0));
                dial.setAction(ROOT_URL + "/afterDial");
                dial.setRecord("record-from-answer");
                twiml.append(dial);
            } else {
                Say say = new Say("Your input of " + readDigits(call.getMls()) +
                        " could not be found .");
                Say pleaseTryAgain = new Say(" please try again.");
                say.setVoice("woman");
                pleaseTryAgain.setVoice("woman");
                twiml.append(say);
                if (dataService.getCallCount(caller) < 3) {
                    twiml.append(pleaseTryAgain);
                    sfService.logNotFound(call, "MLS could not be found", false);
                    dataService.removeFromDigitsQueue(call.getCaller());
                }
                twiml.append(new Redirect(ROOT_URL + "/welcome"));
            }
        } catch (TwiMLException e) {
            logger.fatal(e.getMessage());
        }
        return twiml;
    }

    public TwiMLResponse noResponseAtWelcome() {
        return sendToVoicemail("We have not received a response . ");
    }

    public TwiMLResponse sendToVoicemail(String reason) {
        TwiMLResponse twiml = new TwiMLResponse();
        Say say = new Say(reason + " After the tone, " +
                "Please leave a voice mail detailing your request . . ");
        say.setVoice("woman");
        try {
            twiml.append(say);
            Record record = new Record();
            record.setAction(ROOT_URL + "/voicemail");
            record.setPlayBeep(true);
            twiml.append(record);
        } catch (TwiMLException e) {
            logger.fatal(e.getMessage());
        }
        return twiml;
    }

    public TwiMLResponse afterDial() {

        TwiMLResponse twiml = new TwiMLResponse();
        try {
            twiml.append(new Hangup());
        } catch (TwiMLException e) {
            logger.fatal(e.getMessage());
        }
        return twiml;
    }

    public String readDigits(String digits) {
        return digits.replace("", " . ");
    }

}
