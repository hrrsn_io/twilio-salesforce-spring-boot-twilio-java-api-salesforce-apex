package us.foxconsulting.ttsf.dao;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by hgm12 on 1/3/2016.
 */
@Component
public class SalesforceDao {

   /* @Autowired
    SFConnectionFactory connectionFactory;*/

    @Value("${salesforceMaxTries}")
    Integer maxTies;

    private final Logger logger = LogManager.getLogger();

    //    private final String SELECT_ON_ENGAGEMENT = "select Seller__r.Phone__c from Offering__c where MLS_short__c = '";
    private final String SELECT_ON_ENGAGEMENT = "select id, Smart_Phone_Number__r.X10_Digits__c from Engagement__c where SmartAd_Active__c = true and  MLS_Number_Short__c = '";

    public QueryResult selectOnEngagement(String digits) {
        QueryResult queryResults = new QueryResult();
        while (true) {
            NoBeanSFConnection noBeanSFConnection = new NoBeanSFConnection();
            int tries = 1;
            try {
                queryResults = noBeanSFConnection.getNoBeanConnection().query(SELECT_ON_ENGAGEMENT + digits + "'");
                break;
            } catch (Exception e) {
                tries++;
                e.printStackTrace();
                if (tries > maxTies) {
                    logger.fatal("max tries reached");
                    break;
                }
            }
        }
        return queryResults;
    }

    public void insertSfObject(SObject[] sObjArray) {
        SaveResult[] queryResults;
        while (true) {
            NoBeanSFConnection noBeanSFConnection = new NoBeanSFConnection();
            int tries = 1;
            try {
                queryResults = noBeanSFConnection.getNoBeanConnection().create(sObjArray);
                StringBuilder sb = new StringBuilder();
                for (SaveResult result : queryResults) {
                    sb.append(result.toString() + " ");
                }
                if (sb.toString().contains("success='false'")) {
                    logger.error(sb.toString());
                } else logger.info(" $insert$ " + sb.toString());
                break;
            } catch (Exception e) {
                tries++;
                e.printStackTrace();
                if (tries > maxTies) {
                    logger.fatal("max tries reached");
                    break;
                }
            }
        }
    }

    public QueryResult selectOnProperty(String digits) {
        NoBeanSFConnection noBeanSFConnection = new NoBeanSFConnection();
        QueryResult queryResults = null;
        try {
            queryResults = noBeanSFConnection.getNoBeanConnection().query("SELECT Address_1__c, Agent_Number__c " +
                    "from Property__c where MLS__c ='" + digits + "'");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return queryResults;
    }

    public QueryResult selectOnCallLog() {
        NoBeanSFConnection noBeanSFConnection = new NoBeanSFConnection();
        QueryResult queryResults = null;
        try {
            //queryResults = connectionFactory.getConnection().query("SELECT Account_SID__c, API_Version__c, Attempted_MLS__c, Callback_Source__c, Call_Duration__c, Called__c, Called_City__c, Called_Country__c, Called_State__c, Called_Zip__c, Caller__c, Caller_City__c, Caller_Country__c, Caller_State__c, Caller_Zip__c, Call_SID__c, Call_Status__c, Direction__c, Duration__c, From__c, From_City__c, From_Country__c, From_State__c, From_Zip__c, MLS__c, MLS_Found__c, rel_Property__c, Sequence_Number__c, Timestamp__c, To__c, To_City__c, To_Country__c, To_State__c, To_Zip__c from Engagement__c");
            queryResults = noBeanSFConnection.getNoBeanConnection().query("SELECT From__c from SmartAd__c");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return queryResults;
    }
}
