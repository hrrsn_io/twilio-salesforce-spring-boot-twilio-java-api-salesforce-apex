package us.foxconsulting.ttsf.dao;

import com.sforce.soap.partner.Connector;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.ws.ConnectionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 * Created by hgm12 on 1/5/2016.
 */
@Repository
@Scope("prototype")
public class NoBeanSFConnection {

//    private String username = "smartad@dorothy.com";
//    private String password = "harrison1o2S77qSdwrSprpoWfYhnNvyS";

    @Value("${salesforceUsername}")
    private String username;

    @Value("${salesforcePasswordToken}")
    private String password;


    private final Logger logger = LogManager.getLogger();

    public PartnerConnection getNoBeanConnection() {
        try {
            System.out.println("username @@@@ " + username);
            System.out.println("password @@@@ " + password);
            Connector connector = new Connector();
            PartnerConnection connection = connector.newConnection(username, password);
            return connection;
        } catch (ConnectionException e) {
            logger.fatal("Could not get");
            e.printStackTrace();
        }
        return null;
    }


    public void loginAgain(PartnerConnection connection) {
        try {
            connection.login(username, password);
            logger.info("new sf login");
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }

    public void logout(PartnerConnection connection) {
        try {
            connection.logout();
            logger.info("sf logout");
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }
}
