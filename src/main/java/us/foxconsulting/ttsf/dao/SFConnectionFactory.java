package us.foxconsulting.ttsf.dao;

import com.sforce.soap.partner.Connector;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

/**
 * Created by hgm12 on 1/5/2016.
 */

public class SFConnectionFactory {

    private PartnerConnection connection;

    @Value("${salesforceUsername")
    private String username;

    @Value("${salesforcePasswordToken")
    private String password;

    private final Logger logger = LogManager.getLogger();

    SFConnectionFactory(Connector connector, ConnectorConfig config) {
        try {
            connection = connector.newConnection(config);
        } catch (Exception e) {
            //logger.fatal(e.getMessage());
            e.printStackTrace();
        }
    }

    public PartnerConnection getConnection() {
        return connection;
    }

    public void loginAgain() {
        try {
            connection.login(username, password);
            logger.info("new sf login");
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }
     public void logout() {
        try {
            connection.logout();
            logger.info("new sf login");
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }
}
