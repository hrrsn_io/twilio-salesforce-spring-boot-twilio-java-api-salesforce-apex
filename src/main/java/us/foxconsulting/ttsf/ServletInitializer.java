package us.foxconsulting.ttsf;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by hgm12 on 3/5/2016.
 */
@SpringBootApplication
@ComponentScan(basePackages = { "us.foxconsulting.ttsf.*" })
@PropertySource("classpath:ttsf.properties")
public class ServletInitializer extends SpringBootServletInitializer {
    private final static Logger log = LogManager.getLogger();

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources("classpath:application-context.xml");
    }

   /* public static void main(String[] args) {
        log.info("App startup");

//        ApplicationContext ctx = SpringApplication.run(ServletInitializer.class, args);

       *//* TwilioRestClient client = ctx.getBean(TwilioRestClient.class);

        SFConnectionFactory ss = ctx.getBean(SFConnectionFactory.class);
        SalesforceService sf = ctx.getBean(SalesforceService.class);

        try {
            System.out.println(ss.getConnection().getUserInfo().getUserEmail());
            sf.getSellerInfo("999999");
            System.out.println(client.getAccountSid());
        } catch (ConnectionException e) {
            e.printStackTrace();
        }*//*

        //ss.getSellerInfo();
        *//*System.out.println("Let's inspect the beans provided by Spring Boot:");

        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        for (String beanName : beanNames) {
            System.out.println(beanName);
        }*//*

        //log.fatal("test err");
    }*/

}
